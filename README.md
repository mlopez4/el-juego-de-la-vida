# [El Juego de la vida](https://es.wikipedia.org/wiki/Juego_de_la_vida) 
## de [John Horton Conway](https://es.wikipedia.org/wiki/John_Horton_Conway)

Este es un proyecto que voy perfeccionando en mis ratos libres, al margen del ciclo formativo.

Se puede ver en funcionamiento desde [aqui](https://mlopez4.gitlab.io/el-juego-de-la-vida)