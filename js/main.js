// contenedor donde pintaremos el tablero
var container = document.getElementsByClassName("container")[0];

// allamos la medida de la ventana del usuario
var vAlto = window.innerHeight - 40; // retamos 40px para que el boton "Start" no nos desplace el tablero
var vAncho = window.innerWidth;


// escuchador de evento del boton start
document.getElementById("boton").addEventListener("click", function(){
    setInterval('ejecutarRonda()',500);
});

// conjunto de funciones que se ejecutan en cada ronda
function ejecutarRonda(){
    comprobarCelulasArays();
    dibujarTablero();
    copiarArrayTablero();
}



//pedimos las dimensiones del tablero
var alto = parseInt(prompt("Inserta la altura:"));
var ancho = parseInt(prompt("Inserta el ancho:"));





// plantillas de arrays para variar el tablero en cada vuelta
var valores1 = new Array(alto);
var valores2 = new Array(alto);



// inicializamos los arrays con el ancho y el alto de las variables de mas arriba
for (var i=0; i<valores1.length; i++){
    valores1[i] = new Array(ancho);
    valores2[i] = new Array(ancho);
}


// Dibujar tablero en blanco i ponemos los valores de los arrays a "0"
// Ponemos eventos a cada celula para poder marcarlas como vivas y enlazar el patron con
// el array valores1
for (var i=0; i<valores1.length; i++){
    // creamos el contenedor fila
    var row = document.createElement("div");
    row.className += "row";

    for(var j=0; j<valores1[0].length; j++){
        
        //valores de los arrays a 0
        valores1[i][j] = 0;
        valores2[i][j] = 0;

        // creamos la celulas
        var cell = document.createElement("div");
        cell.id = (i)+"-"+(j);
        cell.className += "cell";


        // adaptamos el tablero a la pantalla 
        // (asignamos alto y ancho de celula dependiendo del ancho de ventana del usuario)
        cell.style = "height: "+(vAlto/alto)+"px;"+" "+"width: "+(vAncho/ancho)+"px;";

        cell.addEventListener("click", function(){

            var coordenadasArrays = this.id.split("-");

            if (this.className == "cell"){
                this.className += " on";
                valores1[coordenadasArrays[0]][coordenadasArrays[1]] = 1;
            } else {
                this.className = "cell";
                valores1[coordenadasArrays[0]][coordenadasArrays[1]] = 0;
            }
        })
        row.appendChild(cell);
    }
    container.appendChild(row);
}




// revivir o matar celulas (solo de los arrays) segun el estado de las celulas vecinas
// comprueba en el array origen y actualiza en el array destino
function comprobarCelulasArays(){

    // ***** hay que reparar problema con el perimetro exteriror (se sale de rango al comprobar) *****
    
    // calculamos el estado de las celulas (valores del array valores1) y hacemos los cambios pertinentes en el array valorer2
    for (var i=1; i<valores1.length-1; i++){
        for (var j=1; j<valores1[0].length-1; j++){

            // seleccionamos las celulas vecinas
            var cellArray = valores1[i][j];
            var cellArrayArriba = valores1[i-1][j];
            var cellArrayAbajo = valores1[i+1][j];
            var cellArrayDerecha = valores1[i][j+1];
            var cellArrayIzquierda = valores1[i][j-1];

            var cellArrayArribaDerecha = valores1[i-1][j+1];
            var cellArrayAbajoIzquierda = valores1[i+1][j-1];
            var cellArrayAbajoDerecha = valores1[i+1][j+1];
            var cellArrayArribaIzquierda = valores1[i-1][j-1];

            // comprobamos las celulas vecinas vivas
           
                var celulasVivas = 0;
                celulasVivas = (cellArrayArriba == 1) ? celulasVivas+1 : celulasVivas;
                celulasVivas = (cellArrayAbajo == 1) ? celulasVivas+1 : celulasVivas;
                celulasVivas = (cellArrayDerecha == 1) ? celulasVivas+1 : celulasVivas;
                celulasVivas = (cellArrayIzquierda == 1) ? celulasVivas+1 : celulasVivas;

                celulasVivas = (cellArrayArribaDerecha == 1) ? celulasVivas+1 : celulasVivas;
                celulasVivas = (cellArrayAbajoIzquierda == 1) ? celulasVivas+1 : celulasVivas;
                celulasVivas = (cellArrayAbajoDerecha == 1) ? celulasVivas+1 : celulasVivas;
                celulasVivas = (cellArrayArribaIzquierda == 1) ? celulasVivas+1 : celulasVivas;

                /*if (celulasVivas == 3) {
                    valores2[i][j] = 1;
                } else {
                    valores2[i][j] = 0;
                }*/

                if (cellArray == 1){
                    if (celulasVivas<2 || celulasVivas>3){
                        valores2[i][j] = 0;
                    } else {
                        valores2[i][j] = 1;
                    }
                } else {   
                    if (celulasVivas == 3){
                        valores2[i][j] = 1;
                    }                    
                }

                

                
        }
    }
}






// funcion que dibuja el tablero de celulas partiendo de los valores de el array insertado por parametro
function dibujarTablero(){

    for (var i=0; i<valores2.length; i++){
        for(var j=0; j<valores2[0].length; j++){
           if(valores2[i][j] == 1){
               document.getElementById(i+"-"+j).className = "cell on";
           } else {
            document.getElementById(i+"-"+j).className = "cell";
           }
        }
    }

}


// pasa valores de valores2 a valores1 para continuar con el ultimo cambio
function copiarArrayTablero(){
    for (var i=0; i<valores1.length; i++){
        for (var j=0; j<valores1[0].length; j++){
            valores1[i][j] = valores2[i][j]
        }
    }
}
